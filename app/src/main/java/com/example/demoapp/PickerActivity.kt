package com.example.demoapp

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.SeekBar
import android.widget.Toolbar
import kotlinx.android.synthetic.main.activity_picker.*

class PickerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)

        val toolbar = findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = null

        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

//        btnColorPicker.setOnClickListener {
//            colorSelector.visibility = View.VISIBLE
//        }

//        btnColorSelected.setOnClickListener {
//            colorSelector.visibility = View.VISIBLE
//        }






        //colorA.max = 255
//        colorA.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
//            override fun onStopTrackingTouch(seekBar: SeekBar) {}
//            override fun onStartTrackingTouch(seekBar: SeekBar) {}
//            override fun onProgressChanged(seekBar: SeekBar, progress: Int,
//                                           fromUser: Boolean) {
//                val colorStr = getColorString()
//                strColor.setText(colorStr.replace("#","").toUpperCase())
//                colorPreview.setBackgroundColor(Color.parseColor(colorStr))
//            }
//        })

        colorR.max = 255
        colorR.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onProgressChanged(seekBar: SeekBar, progress: Int,
                                           fromUser: Boolean) {
                val colorStr = getColorString()

                colorPreview.setBackgroundColor(Color.parseColor(colorStr))
            }
        })

        colorG.max = 255
        colorG.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onProgressChanged(seekBar: SeekBar, progress: Int,
                                           fromUser: Boolean) {
                val colorStr = getColorString()

                colorPreview.setBackgroundColor(Color.parseColor(colorStr))
            }
        })

        colorB.max = 255
        colorB.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onProgressChanged(seekBar: SeekBar, progress: Int,
                                           fromUser: Boolean) {
                val colorStr = getColorString()

                colorPreview.setBackgroundColor(Color.parseColor(colorStr))
            }
        })

//        colorCancelBtn.setOnClickListener {
//            colorSelector.visibility = View.GONE
//        }

//        colorOkBtn.setOnClickListener {
//            val color:String = getColorString()
//            btnColorSelected.setBackgroundColor(Color.parseColor(color))
//            colorSelector.visibility = View.GONE
//        }
    }
    fun getColorString(): String {
//        var a = Integer.toHexString(((255*colorA.progress)/colorA.max))
//        if(a.length==1) a = "0"+a
        var r = Integer.toHexString(((255*colorR.progress)/colorR.max))
        if(r.length==1) r = "0"+r
        var g = Integer.toHexString(((255*colorG.progress)/colorG.max))
        if(g.length==1) g = "0"+g
        var b = Integer.toHexString(((255*colorB.progress)/colorB.max))
        if(b.length==1) b = "0"+b
        //return "#" + a + r + g + b
        return ("#"+ r + g + b)
    }
    //}
}
