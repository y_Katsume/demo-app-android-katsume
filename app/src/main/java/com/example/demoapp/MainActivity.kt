package com.example.demoapp


import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.demoapp.MyAdapter.OnItemClickListener
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var myDataset: ArrayList<String> = ArrayList()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        addList()

        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)

        viewManager = LinearLayoutManager(this)
        viewAdapter = MyAdapter(myDataset)
        val adapter = MyAdapter(myDataset)

//        val dividerItemDecoration = DividerItemDecoration(
//            recyclerView.context,
//            (viewManager as LinearLayoutManager).orientation
//        )
//        recyclerView.addItemDecoration(dividerItemDecoration)

        recyclerView = findViewById<RecyclerView>(R.id.my_recycler_view).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)
            // use a linear layout manager
            layoutManager = viewManager
            // specify an viewAdapter (see also next example)
            //adapter = viewAdapter

            my_recycler_view.layoutManager = layoutManager
            my_recycler_view.adapter = adapter
            my_recycler_view.setHasFixedSize(true)
            my_recycler_view.addItemDecoration(itemDecoration)

            adapter.setOnItemClickListener(object: OnItemClickListener{
                override fun onItemClickListener(view: View, position: Int, clickedText: String) {
                    //val intent = Intent(this@MainActivity, MapsActivity::class.java)
                    //生成したオブジェクトを引数に画面を起動！
                    //startActivity(intent)
                    //Log.d("$clickedText", "です")

                    if(clickedText == "Slider") {
                        val intent = Intent(this@MainActivity, PickerActivity::class.java)
                        startActivity(intent)
                    }else if(clickedText == "Map View"){
                        val intent = Intent(this@MainActivity, MapsActivity::class.java)
                        startActivity(intent)
                    }else if(clickedText == "Play Video") {
                        val intent = Intent(this@MainActivity, VideoActivity::class.java)
                        startActivity(intent)
                    }

                }
            })
        }
    }
    private fun addList() {
        myDataset.add("Slider")
        myDataset.add("Map View")
        myDataset.add("Play Video")

    }
}